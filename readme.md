
## About Sheba 

Sheba is a Ride shearing app. This is the admin panel of sheba.

## Team Member

Sheba App is developed by Sami and Taohid. Also api end is supported by Mehedi

## Challanges

Them main Challange of Sheba Ride Shearing apps is introduce NoSql database as MongoDb with laravel.
All the challanges and their solve given below : 
1. Most Difficult part was integrating Passport (OAuth2 api auth) with multi auth also with Mongodb.
  
## Solve

<u>Solve Challange 1 :</u> <br> <br>
There will be two requirements. One is Mongodb have to supported with passport.
As by default passport is written for mysql. so we have to edit in core.<br>
After install passport we will replace the laravel default eloquent setup with Jenssegers Mongodb eloquent by using<br>
<a href="https://github.com/sadnub/laravel-mongodb-passport-fix">laravel mongodb passport fix</a><br>
It will enable us to run passport:install and other query with mongodb support .
By default Passport search for "id" in <br>
/vendor/laravel/passport/src/ClientRepository<br>
but in mongodb there is no "id", it will be "_id". So preplace the "id" to "_id" in this file.

But in   /vendor/laravel/passport/src/TokenRepository use oauth_access_tokens collection which use "id" so in this file "id" will not be replaced.

<br>Now 2nd Problem. Using passport for multi auth.<br>
In /vendor/laravel/passport/src/Bridge/UserRepository.php must be change a method name <br>
getUserEntityByUserCredentials() with a new parameter <i>$newProvider</i><br>
and change the line 
<br>
$provider = config('auth.guards.api.provider');  to >>>>>>>>> <br>
$provider = config('auth.guards.'.$newProvider.'.provider');
<br>
Now in /vendor/league/oauth2-server/src/Grant/PasswordGrant.php, in validateUser() method add the line<br>

        $newProvider = $this->getRequestParameter('newProvider', $request);
        if (is_null($newProvider)) {
            throw OAuthServerException::invalidRequest('newProvider');
        }

Now add a extra parameter <i>$newProvider</i> in same method's this line :   $user = $this->userRepository->getEntityByUserCredentials()

        $user = $this->userRepository->getUserEntityByUserCredentials(
                    $username,
                    $password,
                    $this->getIdentifier(),
                    $client,
                    $newProvider
                );   

Also add extra two element in <i>auth</i> array in config/auth file 
        
    'guards' => [

        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],

        'driver' => [
            'driver' => 'passport',
            'provider' => 'drivers',
        ],

        'customer' => [
            'driver' => 'passport',
            'provider' => 'customers',
        ],
    ],
    
    
Also add two providers with their eloquent class in provider array 


        'providers' => [
            'users' => [
                'driver' => 'eloquent',
                'model' => App\User::class,
            ],
    
            'drivers' => [
                'driver' => 'eloquent',
                'model' => App\Driver::class,
            ],
    
            'customers' => [
                'driver' => 'eloquent',
                'model' => App\Customer::class,
            ],
        ];
        
By thus the passport with mongodb and multi auth start working