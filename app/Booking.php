<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Booking extends Eloquent
{

   public static function getCarNameFromCarType($carTypeId)
   {
       $carType = DB::collection('car_types')->where('id', $carTypeId)->first();

       return isset($carType['name']) ? $carType['name'] : '';  // have to use this formate for Jenssegers mongodb
   }


   public static function getPatternNameFromPattern($patternId)
   {
       $patternData = DB::collection('pattern')->where('id', $patternId)->first();

       return isset($patternData['pattern_name']) ? $patternData['pattern_name'] : '';// have to use this formate for Jenssegers mongodb
   }

    public static function getBookingStatus($status){

        $bookingArray = [
            '1'=>'Booking',
            '2'=>'Processing',
            '3'=>'Completed',
            '0'=>'Cancelled'
        ];

        return $bookingArray[$status];
    }

    public static function getCssColor($status){

        $cssArray = [
            '1'=>'info',
            '2'=>'warning',
            '3'=>'success',
            '0'=>'danger'
        ];

        return $cssArray[$status];
    }

}
