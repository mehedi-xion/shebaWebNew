<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Setting extends Eloquent
{



    public static function getHeaderText(){

        $setting = Setting::first();
        return $setting->header_title;
    }


    public static function getFooterText(){

        $setting = Setting::first();
        return $setting->footer_text;
    }

    public static function getLogo(){

        $setting = Setting::first();
        return asset('storage/images/'.$setting->logo);
    }
}
