<?php

namespace App\Http\Resources\CustomerResource;

use App\Customer;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerSingleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'user_code' => $this->user_code,
            'otp' => $this->otp,
            'reference_id' => Customer::refererCustomer($this->reference_id),
            'number_of_bookings' => Customer::getNumberOfBooking($this->id),
            'affiliate_code' => $this->affiliate_code,
            'promocode' => $this->promocode,
            'image' => '<img src="'.$this->image.'" class="img-responsive" />',

        ];
    }
}
