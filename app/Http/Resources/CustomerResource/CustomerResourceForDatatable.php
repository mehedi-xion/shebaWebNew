<?php

namespace App\Http\Resources\CustomerResource;

use App\Customer;
use App\Helper\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResourceForDatatable extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'id' => $this->id,
            'name' => $this->name,
            'affiliate_code' => $this->affiliate_code,
            'customer_affiliate_count' => Helpers::getCustomerAffiliateCount($this->affiliate_code),
            'driver_affiliate_count' => Helpers::getDriverAffiliateCount($this->affiliate_code),
            'phone' => $this->phone,
            'number_of_bookings' => Customer::getNumberOfBooking($this->id),
            'status' => Helpers::getStatusName($this->status),
            'label_colors' => Helpers::getCssColor($this->status),
        ];
    }
}
