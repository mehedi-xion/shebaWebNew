<?php

namespace App\Http\Resources\FeedbackResource;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackSingleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'booking_id' => $this->booked_id,
            'customer' => $this->customer_name,
            'driver' => $this->driver_name,
            'source' => $this->booking->source,
            'destination' => $this->booking->destination,
            'rating' => $this->rating,
            'good_feedback' => $this->good_feedback,
            'bad_feedback' => $this->bad_feedback,
            'driver_feedback' => $this->driver_feedback,
            'trip_report' => $this->trip_report,
        ];
    }
}
