<?php

namespace App\Http\Resources\DriverResource;

use App\Driver;
use App\Helper\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers;

class DriverResourceForDatatable extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'driver_name' => $this->driver_name,
            'ride_model' => $this->ride_model,
            'ride_name' => isset($this->rideType->name) ? $this->rideType->name : '',
            'balance_amount' => $this->amount,
            'affiliate_code' => $this->affiliate_code,
            'driver_rating' => Driver::getDriverRating($this->id),
            'customer_affiliate_count' => Helpers::getCustomerAffiliateCount($this->affiliate_code),
            'driver_affiliate_count' => Helpers::getDriverAffiliateCount($this->affiliate_code),
            'phone' => $this->phone,
            'status' => Helpers::getStatusName($this->status),
            'label_colors' => Helpers::getCssColor($this->status),
        ];
    }
}
