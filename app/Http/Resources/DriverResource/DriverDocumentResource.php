<?php

namespace App\Http\Resources\DriverResource;

use App\DriverDocument;
use App\Helper\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;
use MongoDB\BSON\ObjectId;


class DriverDocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'driver_name' => $this->driver_name,
            'driver_document' => $this->documentTypePrint($this->driver_documents),
        ];
    }

    public function documentTypePrint($documents)
    {
        $documentTypeHtml = '';

        $serial = 1;
        foreach ($documents as $document) {
            $bgCss = 'btn-' . $this->getCssColorByType($document['status']);
            $documentTypeHtml .= '<span data-toggle="modal" data-target="#singleDocumentModal" data-id="'.$document['_id'].'" class="btn singleDocument ' . $bgCss . ' m-l-xs m-b-xs btn-xs">' . DriverDocument::documentTypeName($document['type']) . '</span>';

            if ($serial % 6 == 0) {
                $documentTypeHtml .= '<br>';
            }
            $serial++;
        }

        return $documentTypeHtml;
    }

    public function getCssColorByType($status)
    {
        $array = DriverDocument::getAllCssColorByDocumentStatus();
        return $array[$status];
    }
}
