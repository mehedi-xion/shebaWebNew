<?php

namespace App\Http\Resources\DriverResource;

use App\Driver;
use App\Helper\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverSingleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->driver_name,
            'ride_name' => $this->rideType->name,
            'ride_model' => $this->ride_model,
            'vehicle_registration' => $this->vehicle_reg_no,
            'vehicle_license' => $this->license_no,
            'phone' => $this->phone,
            'email' => $this->email,
            'post_code' => $this->post_code,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'deaf_condition' => Helpers::getYesOrNo($this->is_deaf),
            'flash_required_condition' => Helpers::getYesOrNo($this->is_flash_required),
            'driver_type' => Driver::getDriverType($this->driver_type),
            'image' => '<img src="'.asset('storage/driver/'.$this->image).'" style="width: 130px;height:auto;" class="img-responsive" />',
        ];
    }
}
