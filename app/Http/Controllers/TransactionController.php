<?php

namespace App\Http\Controllers;

use App\Balance;
use App\BalanceTransaction;
use App\Driver;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * show transaction page
     */
    public function index(){

        return view('admin.transactions.all_transaction');
    }

    /**
     * show transaction in json formate
     */
    public function allTransactionJson()
    {

        return response()->json([

            'aaData' => BalanceTransaction::all()

        ]);

    }


    /**
     * show transaction in json formate
     */
    public function showPaymentPage(Driver $driver)
    {

        return view('admin.transactions.create_payment')->with('driver', $driver);

    }


    /**
     * show transaction in json formate
     */
    public function paymentCalculate(Request $request, Driver $driver)
    {
        $oldBalance = isset($driver->balance->amount) ? $driver->balance->amount : 0;

        $newAmount = $request->amount;

        if($request->invoice_type == 2){ //Pay To Driver
            $newAmount = -1 * $newAmount; // make this negetive

            $transPurpose = 'Debit Transaction of Driver';
        }else{
            $transPurpose = 'Credit Transaction of Driver';
        }

        $balance = $oldBalance + $newAmount;

        /*
         * Update if Balance Table has data belongs to
         * this driver.
         * If Balance Table does not have this driver's data
         * then create new row in table.
         */
        if(isset($driver->balance->amount)){

            $driver->balance->amount = $balance;
            $driver->balance->save();

        }else{

            $balanceTable = new Balance();

            $balanceTable->amount = $balance;
            $balanceTable->user_id = $driver->_id;
            $balanceTable->user_id_type = 'Driver';

            $balanceTable->save();

        }

        $balanceTransaction = new BalanceTransaction();

        $balanceTransaction->trans_amount = intval($newAmount);
        $balanceTransaction->trans_purpose = $transPurpose;
        $balanceTransaction->trans_user_type = 'Driver';
        $balanceTransaction->comments = $request->comments;
        $balanceTransaction->trans_date = Carbon::now();

        $balanceTransaction->save();

        return redirect()->route('driver.payment.create', $driver->_id)->with('success', 'Drivers Payment Assigned Successfully');

    }
}
