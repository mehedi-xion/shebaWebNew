<?php

namespace App\Http\Controllers;

use App\Booking;
use App\RideType;
use App\Http\Resources\BookingResource\BookingsResourceForDatatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    /**
     * Display all bookings
     */
    public function index()
    {

        return view('admin.bookings.all_bookings');

    }

    /**
     * Display all bookings
     */
    public function allBookingsJson()
    {

        return response()->json([

            'aaData' => BookingsResourceForDatatable::collection(Booking::all())

        ]);

    }

    /**
     * Display all Splited bookings
     */
    public function showSplitBooking($parameter)
    {

        if ($parameter == 'completed' || $parameter == 'processing' || $parameter == 'cancelled') {

            return view('admin.bookings.splited_booking')->with('parameter', $parameter);

        } else {
            return abort(404);
        }


    }

    /**
     * Display all bookings
     */
    public function returnSplitBookingJson($parameter)
    {
        if ($parameter == 'completed') {
            $status = 3;

        } else if ($parameter == 'processing') {
            $status = 2;

        } else if ($parameter == 'cancelled') {
            $status = 0;
        } else {
            return abort(404);
        }

        return response()->json([

            'aaData' => BookingsResourceForDatatable::collection(Booking::where('status', $status)->get())

        ]);

    }

}
