<?php

namespace App\Http\Controllers;

use App\Request;

class RequestController extends Controller
{

    public function index(){

        return view('admin.requests.index');
    }


    public function getRequestsJson(){

        return response()->json([

            'aaData' => Request::orderBy('_id', 'desc')->get()

        ]);
    }


    public function show(Request $request){

        $returnRequest = [
            'driver_name' => $request->driver_name,
            'customer_name' => $request->customer_name,
            'ride' => $request->ride_type,
            'source' => $request->source,
            'destination' => $request->destination,
            'trip_fare' => $request->total_amount.' tk',
            'status' => Request::getRequestStatus($request->status),
        ];
        return response()->json($returnRequest);
    }


}