<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        if(Gate::allows('read')) {
            echo "read<br>";
        }
        if(Gate::allows('write')) {
            echo "write<br>";
        }
    }



    public function create()
    {
        //
    }



    public function store(Request $request)
    {
        //
    }



    public function show($id)
    {
        //
    }



    public function edit($id)
    {
        //
    }




    public function update(Request $request, $id)
    {
        //
    }



    public function destroy($id)
    {
        //
    }
}
