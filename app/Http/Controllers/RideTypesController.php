<?php

namespace App\Http\Controllers;

use App\RideTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Validator;

class RideTypesController extends Controller
{
    /**
     * Display a listing of the RideType.
     *
     */
    public function index()
    {

        $rides = RideTypes::all();

        return view('admin.rides.index')->with('rides', $rides);
    }

    /**
     * Show the form for creating a new RideType.
     *
     */
    public function create()
    {
        return view('admin.rides.create');
    }

    /**
     * Store a newly created RideType in storage.
     *
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $ride = new RideTypes();
        $ride->name = $request->name;
        $ride->max_seat = intval($request->max_seat);
        $ride->fare = intval($request->fare);
        $ride->min_fare = intval($request->min_fare);
        $ride->km_fare = intval($request->km_fare);
        $ride->time_fare = intval($request->time_fare);
        $ride->status = intval($request->status);
        $ride->deleted_at = null;

        if ($request->hasFile('image')) {

            $ride->image = $this->uploadImage($request->file('image'));

        } else {

            $ride->image = "";
        }

        $ride->save();

        return redirect()->route('rides.create')->with('success', 'Rides Successfully Created');

    }

    /**
     * Display the specified RideType.
     *
     */
    public function show(RideTypes $ride)
    {
        $ride['ride_images'] = '<img src="' . $ride->image . '" class="img-responsive" />';
        unset($ride->image);
        unset($ride->_id);
        unset($ride->id);
        return response()->json($ride);
    }

    /**
     * Show the form for editing the specified RideType.
     *
     */
    public function edit(RideTypes $ride)
    {
        return view('admin.rides.edit')->with('ride', $ride);
    }

    /**
     * Update the specified RideType in Database.
     *
     */
    public function update(Request $request, RideTypes $ride)
    {

        $this->validator($request->all())->validate();

        $ride->name = $request->name;
        $ride->max_seat = intval($request->max_seat);
        $ride->fare = intval($request->fare);
        $ride->min_fare = intval($request->min_fare);
        $ride->km_fare = intval($request->km_fare);
        $ride->time_fare = intval($request->time_fare);
        $ride->status = intval($request->status);

        if ($request->hasFile('image')) {

            $imagePath = storage_path('app/public/ride/' . $ride->image);

            if (File::exists($imagePath)) {
                File::delete($imagePath);
            }

            $ride->image = $this->uploadImage($request->file('image'));

        }

        $ride->save();

        return redirect()->route('rides.edit', $ride->_id)->with('success', 'Rides Successfully Updated');

    }

    /**
     * use Soft delete to the specified RideType from Database.
     *
     */
    public function destroy(RideTypes $ride)
    {
        $ride->delete();
        return redirect()->route('rides.index')->with('success', 'Rides Successfully Deleted');
    }
    /**
     * Validate the request.
     *
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
            'name' => 'required|string',
            'fare' => 'required|numeric',
            'min_fare' => 'required|numeric',
            'km_fare' => 'required|numeric',
            'time_fare' => 'required|numeric',
            'status' => 'required|numeric',
            'image' => 'image',
        ]);
    }

    /**
     * upload image to storage/app/public/ride
     */
    public function uploadImage($image){

        $imageName = uniqid() . time() . '.' . $image->getClientOriginalExtension();

        Image::make($image)->resize(300, null, function ($constraint) {

            $constraint->aspectRatio();

        })->save(storage_path('app/public/ride/' . $imageName));

        return $imageName;
    }
}
