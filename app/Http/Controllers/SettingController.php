<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{


    public function edit(){

        $settings = Setting::first();

        return view('admin.settings.edit')->with('settings', $settings);
    }


    public function update(Request $request, Setting $setting){

        $this->validator($request->all())->validate();

        $setting->title = $request->title;
        $setting->header_title = $request->header_title;
        $setting->footer_text = $request->footer_text;
        $setting->tax = $request->tax;
        $setting->admin_charge = $request->admin_charge;
        $setting->booking_code = $request->booking_code;
        $setting->browser_api_key = $request->browser_api_key;

        if ($request->hasFile('image')) {
            $imagePath = storage_path('app/public/images/'. $setting->logo);

            if (File::exists($imagePath)) {
                File::delete($imagePath);
            }

            $setting->logo = $this->uploadImage($request->file('image'));;
        }

        $setting->save();

        return redirect()->route('settings.edit')->with('success', 'Settings Successfully Updated');
    }



    /**
     * Validate the request.
     *
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
                'title' => 'required',
                'header_title' => 'required',
                'footer_text' => 'required',
                'tax' => 'required|numeric',
                'admin_charge' => 'required|numeric',
                'booking_code' => 'required',
                'browser_api_key' => 'required',
                'image' => 'image|max:1000',
            ]);
    }



    /**
     * upload image to storage/app/public/ride
     */
    public function uploadImage($image){

        $imageName = uniqid() . time() . '.' . $image->getClientOriginalExtension();

        Image::make($image)->resize(200, null, function ($constraint) {

            $constraint->aspectRatio();

        })->save(storage_path('app/public/images/' . $imageName));

        return $imageName;
    }



}
