<?php

namespace App\Http\Controllers;

use App\Country;
use App\Pattern;
use App\RideTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PatternController extends Controller
{
    /**
     * Display a listing of the pattern.
     *
     */
    public function index()
    {

        $patterns = Pattern::all();

        return view('admin.patterns.index')->with('patterns', $patterns);
    }

    /**
     * Show the form for creating a new pattern.
     *
     */
    public function create()
    {
        $rideTypes = RideTypes::all();
        return view('admin.patterns.create')->with('rideTypes', $rideTypes);
    }

    /**
     * Store a newly created pattern in storage.
     *
     */
    public function store(Request $request)
    {

        $this->validator($request->all())->validate();

        $pattern = new Pattern();

        $pattern->name = $request->name;
        $pattern->ride_type_id = $request->ride_type_id;
        $pattern->range = floatval($request->range);
        $pattern->base_price = floatval($request->base_price);
        $pattern->min_ride_fare = floatval($request->min_ride_fare);
        $pattern->km_rate = floatval($request->km_rate);
        $pattern->location_string = $request->location;
        $pattern->location = [ // for mongodb geo location
            'type' => 'Point',
            'coordinates' => [
                floatval($request->latitude), floatval($request->longitude)
            ]
        ];
        $pattern->min_rate = floatval($request->min_rate);
        $pattern->currency = $request->currency;
        $pattern->status = intval($request->status);
        $pattern->deleted_at = null;

        $pattern->save();

        return redirect()->route('patterns.create')->with('success', 'Pattern Successfully Created');
    }

    /**
     * Show the form for editing the specified pattern.
     *
     */
    public function edit(Pattern $pattern)
    {
        $rideTypes = RideTypes::all();
        
        $pattern['latitude'] = $pattern->location['coordinates'][0];
        $pattern['longitude'] = $pattern->location['coordinates'][1];
        unset($pattern->location);

        return view('admin.patterns.edit', compact(['rideTypes', 'pattern']));
    }

    /**
     * Update the specified pattern in storage.
     *
     */
    public function update(Request $request, Pattern $pattern)
    {

        $this->validator($request->all())->validate();

        $pattern->name = $request->name;
        $pattern->ride_type_id = $request->ride_type_id;
        $pattern->range = floatval($request->range);
        $pattern->base_price = floatval($request->base_price);
        $pattern->min_ride_fare = floatval($request->min_ride_fare);
        $pattern->km_rate = floatval($request->km_rate);
        $pattern->location_string = $request->location;
        $pattern->location = [ // for mongodb geo location
            'type' => 'Point',
            'coordinates' => [
                floatval($request->latitude), floatval($request->longitude)
            ]
        ];
        $pattern->min_rate = floatval($request->min_rate);
        $pattern->currency = $request->currency;
        $pattern->status = intval($request->status);

        $pattern->save();

        return redirect()->route('patterns.edit', $pattern->_id)->with('success', 'Pattern Successfully Edited');
    }

    /**
     * Remove the specified pattern from storage.
     *
     */
    public function destroy(Pattern $pattern)
    {
        $pattern->delete();
        return redirect()->route('patterns.index')->with('success', 'Patterns Successfully Deleted');
    }

    /**
     * Get country currency
     *
     */
    public function getCurrencyByCountryName($countryName = '')
    {
        $country = Country::getCountryByName($countryName);
        return ($country) ? $country->currrency_symbol : '';
    }



    public function validator(array $requestData)
    {

        return Validator::make($requestData,
            [
                'name' => 'required',
                'ride_type_id' => 'required',
                'location' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'range' => 'required',
                'base_price' => 'required',
                'min_ride_fare' => 'required',
                'km_rate' => 'required',
                'min_rate' => 'required',
                'status' => 'required',
            ]);
    }
}
