<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Customer;
use App\Driver;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * Show the Dashboard with summary data
     */
    public function index()
    {
        $totalSummary['totalRides']  = Booking::where('status',"2")->count();
        $totalSummary['totalBooking']  = Booking::all()->count();
        $totalSummary['totalDriver'] = Driver::all()->count();
        $totalSummary['totalCustomer'] = Customer::all()->count();

        return view('admin.dashboard', $totalSummary);
    }
}
