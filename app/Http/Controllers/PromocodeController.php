<?php

namespace App\Http\Controllers;

use App\Promocode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use MongoDB\BSON\UTCDateTime;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        $promocodes = Promocode::all();

        return view('admin.promocodes.index')->with('promocodes', $promocodes);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.promocodes.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Promocode $promocode)
    {

        return view('admin.promocodes.edit')->with('promocode', $promocode);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Promocode $promocode)
    {

        $this->validator($request->all())->validate();

        $promocode->code = $request->code ;
        $promocode->start_date = new UTCDateTime(strtotime($request->start_date) * 1000) ;
        $promocode->expiration_date = new UTCDateTime(strtotime($request->expiration_date) * 1000) ;
        $promocode->offer = $request->offer ;
        $promocode->status = $request->status ;

        $promocode->save();

        return redirect()->route('promocodes.edit', $promocode->_id)->with('success', 'Promocode Successfully Created');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Validate the requeste
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
                'code' => 'required',
                'start_date' => 'required',
                'expiration_date' => 'required',
                'offer' => 'required',
            ]);
    }
}
