<?php

namespace App\Http\Controllers\V1\DriverEndApi;

use App\Helper\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DriverInfoApiController extends Controller
{

    public function getDriverStatus(){

        if(Auth::user()){

            return Helpers::returnSuccessData(intval(Auth::user()->is_online));
        }
        else{

            return Helpers::returnErrorMsg('209', 'Something went wrong');
        }
    }



    public function updateDriverStatus(Request $request){

        if(empty($request->status) || !isset($request->status)){

            return Helpers::returnErrorMsg('210', 'Provide Driver Status');
        }

        $driver = Auth::user();

        $driver->is_online = intval($request->status);

        if($driver->save()){

            return Helpers::returnSuccessData("Driver Status Updated");
        }

    }
}
