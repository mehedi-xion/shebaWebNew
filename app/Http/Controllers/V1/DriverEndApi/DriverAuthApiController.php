<?php

namespace App\Http\Controllers\V1\DriverEndApi;

use App\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Validator;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;
use Symfony\Component\HttpFoundation\Response;

class DriverAuthApiController extends Controller
{

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:drivers,email',
            'phone' => 'required|numeric|unique:drivers,phone',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $driver = new Driver();
        $driver->driver_name = $request->name;
        $driver->ride_id = $request->ride_id;
        $driver->ride_model = $request->ride_model;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->password = bcrypt($request->password);
        $driver->address = $request->address;
        $driver->post_code = $request->post_code;
        $driver->city = $request->city;
        $driver->state = $request->state;
        $driver->is_deaf = intval($request->is_deaf);
        $driver->is_flash_required = intval($request->is_flash_required);
        $driver->vehicle_reg_no = $request->vehicle_reg_no;
        $driver->license_no = $request->license_no;
        $driver->driver_type = intval($request->driver_type);
        $driver->reference_id = $request->reference_id;
        $driver->status = 0;
        $driver->book_status = 0;
        $driver->is_online = 0;
        $driver->fcm_token = "";
        $driver->affiliate_code = 'DA' . str_random(6);
        $driver->location = [];
        $driver->deleted_at = null;
        if ($driver->save()) {

            $token = $driver->createToken('Sheba Driver Password Grant Client')->accessToken;

            return response([
                'status' => 'success',
                'message' => 'Registration Success of ' . $driver->driver_name,
                'token' => $token,
            ], Response::HTTP_ACCEPTED);

        } else {


            return response([
                'status' => 'error',
                'message' => 'Sorry ! Their is a problem, Try again',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }


    public function login(Request $request)
    {
        if (is_numeric($request->email)) {
            $driver = Driver::where('phone', $request->email)->first();
        } else {
            $driver = Driver::where('email', $request->email)->first();
        }

        if ($driver) {

            if (Hash::check($request->password, $driver->password)) {

                $token = $driver->createToken('Sheba Driver Password Grant Client')->accessToken;

                return response([
                    'status' => 'success',
                    'message' => 'Login Success',
                    'token' => $token,
                ], Response::HTTP_ACCEPTED);

            } else {


                return response([
                    'status' => 'error',
                    'message' => 'Sorry! Password mismatched ! ',
                ], Response::HTTP_UNAUTHORIZED);
            }

        } else {

            return response([
                'status' => 'error',
                'message' => 'Sorry! No Driver found ! ',
            ], Response::HTTP_UNAUTHORIZED);
        }
    }


    public function logout(Request $request)
    {

        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = Token::where('id', $id)->first();
        $token->revoke();

        return response([
            'status' => 'success',
            'message' => 'You have successfully logged out',
        ], Response::HTTP_ACCEPTED);
    }


    public function forgot(Request $request){

        Validator::make($request->all(), ['email' => 'required|email'])->validate();

        if($this->checkEmail($request->email) == 1){

            return $this->sendResetLinkResponse('passwords.sent');
        }else{

            return response([
                'status' => 'error',
                'message' => 'Sorry! Email Address Not Found ! ',
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    protected function checkEmail($email){

        return intval(Customer::where('email', $email)->count());
    }

    protected function sendResetLinkResponse($response)
    {
        return response()->json(['message' => trans($response)]);
    }

    protected function sendResetLinkFailedResponse($response)
    {
        return response()->json(['failed' => $response]);
    }


    public function broker()
    {
        return Password::broker();
    }

    public function reset(Request $request){

    }
}
