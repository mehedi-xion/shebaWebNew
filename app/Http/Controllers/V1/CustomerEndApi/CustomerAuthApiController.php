<?php

namespace App\Http\Controllers\V1\CustomerEndApi;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Validator;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;
use Symfony\Component\HttpFoundation\Response;

class CustomerAuthApiController extends Controller
{

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'phone' => 'required|numeric|unique:customers,phone',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'token' => "",
                'message' => $validator->errors()->first()

            ], Response::HTTP_FORBIDDEN);
        }

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->image = "";
        $customer->user_id = $request->user_id;
        $customer->status = 1;
        $customer->fcm_token = "";
        $customer->promocode = "";
        $customer->affiliate_code = "CA".rand(11,99).$customer->_id;
        $customer->reference_id = isset($request->reference_id) ? $request->reference_id : '';
        $customer->deleted_at = null;

        if ($customer->save()) {

            $token = $customer->createToken('Sheba Customer Password Grant Client')->accessToken;

            return response([
                'success' => true,
                'message' => 'Registration Success of ' . $customer->name,
                'token' => $token,
            ], Response::HTTP_ACCEPTED);

        } else {

            return response([
                'success' => false,
                'message' => 'Sorry ! Their is a problem, Try again',
                'token' => ""
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'token' => "",
                'message' => $validator->errors()->first()

            ], Response::HTTP_FORBIDDEN);
        }


        if (is_numeric($request->email)) {
            $customer = Customer::where('phone', $request->email)->first();
        } else {
            $customer = Customer::where('email', $request->email)->first();
        }

        if ($customer) {

            if (Hash::check($request->password, $customer->password)) {

                $token = $customer->createToken('Sheba Customer Password Grant Client')->accessToken;

                return response([
                    'success' => true,
                    'message' => 'Login Success',
                    'token' => $token,
                ], Response::HTTP_ACCEPTED);

            } else {


                return response([
                    'success' => false,
                    'message' => 'Sorry! Password mismatched ! ',
                    'token' => ""
                ], Response::HTTP_UNAUTHORIZED);
            }

        } else {

            return response([
                'success' => false,
                'message' => 'Sorry! No User found ! ',
                'token' => ""
            ], Response::HTTP_UNAUTHORIZED);
        }
    }


    public function logout(Request $request)
    {
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = Token::where('id', $id)->first();
        $token->revoke();

        return response([
            'success' => true,
            'message' => 'You have successfully logged out',
            'token' => ""
        ], Response::HTTP_ACCEPTED);
    }

    public function forgot(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'token' => "",
                'message' => $validator->errors()->first()

            ], Response::HTTP_FORBIDDEN);
        }

        if($this->checkEmail($request->email) == 1){

           return $this->sendResetLinkResponse('passwords.sent');
        }else{

            return response([
                'success' => false,
                'message' => 'Sorry! Email Address Not Found ! ',
                'token' => ""
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    protected function checkEmail($email){


        return intval(Customer::where('email', $email)->count());
    }

    protected function sendResetLinkResponse($response)
    {
        return response([
            'success' => true,
            'message' => trans($response),
            'data' => null
        ], Response::HTTP_ACCEPTED);
    }

    protected function sendResetLinkFailedResponse($response)
    {
        return response([
            'success' => false,
            'message' => $response,
            'data' => null
        ], Response::HTTP_UNAUTHORIZED);

    }


    public function broker()
    {
        return Password::broker();
    }

    public function reset(Request $request){

    }
}
