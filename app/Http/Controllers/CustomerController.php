<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Resources\CustomerResource\CustomerResourceForDatatable;
use App\Http\Resources\CustomerResource\CustomerSingleResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;


class CustomerController extends Controller
{
    /**
     * Display all Customer
     */
    public function index()
    {

        return view('admin.customers.all_customers');

    }

    /**
     * Display all Customer in json
     */
    public function allCustomersJson()
    {

        return response()->json([

            'aaData' => CustomerResourceForDatatable::collection(Customer::all())

        ]);

    }
    /**
     * Display single Customer in json
     */
    public function singleCustomersJson($id)
    {

        return response()->json( new CustomerSingleResource(Customer::find($id)) );

    }


    public function edit(Customer $customer)
    {

        return view('admin.customers.edit_customer', compact('customer'));
    }


    public function update(Request $request, Customer $customer)
    {
        $this->validator($request->all())->validate();

        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->user_code = $request->user_code;
        $customer->otp = $request->otp;
        $customer->fcm_token = $request->fcm_token;
        $customer->promocode = $request->promocode;
        $customer->status = intval($request->status);

        if ($request->hasFile('image')) {
            $imagePath = storage_path('app/public/customer/'. $customer->image);

            if (File::exists($imagePath)) {
                File::delete($imagePath);
            }

            $customer->image = $this->uploadImage($request->file('image'));;
        }

        if($customer->save()){

            return redirect('/customers/'.$customer->_id.'/edit')->with('success', 'Customer Successfully Updated');
        }else{

            return redirect('/customers/'.$customer->_id.'/edit')->with('error', 'Sorry ! Update Failed');
        }

    }

    public function delete(Customer $customer)
    {
        $customer->delete();
        return redirect()->route('all.drivers')->with('success', $customer->name.' Data Successfully Deleted');

    }


    /**
     * Validate the request.
     *
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
                'name' => 'required',
                'phone' => 'required|unique:users,phone,'.$requestData['phone'],
                'email' => 'required',
                'status' => 'required',
                'image' => 'image|max:1000',
            ]);
    }

    /**
     * upload image to storage/app/public/ride
     */
    public function uploadImage($image){

        $imageName = uniqid() . time() . '.' . $image->getClientOriginalExtension();

        if(!File::exists(storage_path('app/public/customer'))) {

            File::makeDirectory(storage_path('app/public/customer'), $mode = 0777, true, true);
        }

        Image::make($image)->resize(300, null, function ($constraint) {

            $constraint->aspectRatio();

        })->save(storage_path('app/public/customer/' . $imageName));

        return $imageName;
    }

}
