<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
//        'App\User' => 'App\Policies\UserPolicy',
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        Passport::tokensExpireIn(now()->addDay(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));

        Gate::define('write', function ($user) {
            return $user->hasAccess('write');
        });

        Gate::define('read', function ($user) {
            return $user->hasAccess('read');
        });

        Gate::define('update', function ($user) {
            return $user->hasAccess('update');
        });
        Gate::define('delete', function ($user) {
            return $user->hasAccess('delete');
        });

    }
}
