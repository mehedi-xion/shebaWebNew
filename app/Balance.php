<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Balance extends Eloquent
{
    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }
}
