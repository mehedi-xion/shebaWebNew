<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 4/26/2018
 * Time: 11:44 AM
 */

namespace App\Util;


class Constant
{

    const INACTIVE = 0;
    const ACTIVE = 1;
    const DELETED = 2;

}