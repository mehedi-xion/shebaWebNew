<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DriverDocument extends Eloquent
{

    public function driver(){

        return $this->belongsTo(\App\Driver::class);
    }



    public static function getAllCssColorByDocumentStatus(){

        return [
            '0' => 'info',
            '1' => 'primary',
            '2' => 'success',
            '3' => 'danger'
        ];
    }


    public static function getAllStatusAsTextOfDocuments(){

        return [
            '0' => 'Not Defined',
            '1' => 'Pending',
            '2' => 'Approve',
            '3' => 'Rejected'
        ];
    }



    public static function documentTypeName($type=null){

        if(empty($type) || !isset($type)){
            return "";
        }
        $document = [
            0=>'',
            1=>'Driver Licence',
            2=>'Police Clearance Certificate',
            3=>'Fitness Certificate',
            4=>'Vehicle Registration',
            5=>'Vehicle Permit',
            6=>'Commercial Insurance',
            7=>'Tax Receipt',
            8=>'Pass Book',
            9=>'Driver Licence with Badge Number',
            10=>'Background Check with Consent Form',
            11=>'PAN Card',
            12=>'No Objection Certification'
        ];

        return $document[$type];
    }

}
