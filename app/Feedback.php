<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Feedback extends Eloquent
{

    public function booking(){

        return $this->belongsTo(\App\Booking::class, 'trip_id');
    }

    public function customer(){

        return $this->belongsTo(\App\Customer::class);
    }

    public function driver(){

        return $this->belongsTo(\App\Driver::class);
    }
}
