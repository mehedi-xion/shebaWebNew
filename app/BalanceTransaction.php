<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BalanceTransaction extends Eloquent
{
    protected $dates = ['trans_date'];
}
