<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('driver_id');
            $table->integer('car_type');
            $table->string('source');
            $table->string('destination');
            $table->string('source_lat');
            $table->string('source_lng');
            $table->string('destination_lat');
            $table->string('destination_lng');
            $table->string('trip_id', 50);
            $table->mediumInteger('pattern_id');
            $table->string('total_amount');
            $table->string('total_min');
            $table->string('total_km');
            $table->tinyInteger('status')->default(1)->comment('0 - Pending, 1 - Assigned, 2 - Request Failed,3 - Request Cancelled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
