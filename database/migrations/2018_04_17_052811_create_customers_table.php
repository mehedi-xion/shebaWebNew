<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('phone',50);
            $table->string('email',50);
            $table->string('password',50);
            $table->string('image');
            $table->string('user_code');
            $table->tinyInteger('status')->default(1)->comment('0=>Inactive,1=>Active,2=>delete');
            $table->integer('otp');
            $table->text('fcm_token');
            $table->string('promocode', 100);
            $table->string('affiliate_code');
            $table->string('reference_id', 300);
            $table->timestamp('create_date')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
