<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50);
            $table->date('start_date');
            $table->date('expiration_date');
            $table->tinyInteger('type')->default(2)->comment('1=>percentage,2=>fixed');
            $table->integer('off');
            $table->tinyInteger('promo_type')->comment('1=>per_person,2=>broadcast');
            $table->tinyInteger('status')->default(1)->comment('1=>Active,0=>Inactive,2=>delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
