<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id', 50);
            $table->integer('user_id');
            $table->timestamp('book_date');
            $table->tinyInteger('car_type');
            $table->mediumInteger('driver_id');
            $table->double('base_fare');
            $table->double('km_fare');
            $table->double('min_fare');
            $table->double('promotion_fare');
            $table->double('sub_total_fare');
            $table->float('fare');
            $table->double('total_km');
            $table->string('time', 100);
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->string('source');
            $table->string('destination');
            $table->string('distance', 100);
            $table->string('source_lat', 100);
            $table->string('source_lng', 100);
            $table->string('destination_lat', 100);
            $table->string('destination_lng', 100);
            $table->tinyInteger('payment_status')->comment('1=>Succes,0=>Pending');
            $table->tinyInteger('status')->comment('0=>cancelled,1=>booking,2=>inprocess,3=>completed');
            $table->integer('pattern_id');
            $table->mediumInteger('fee');
            $table->mediumInteger('tax');
            $table->mediumInteger('discount');
            $table->mediumInteger('payout');
            $table->tinyInteger('cash_collection')->default(0)->comment('1->Cash Collected, 0->Cash Not collected');
            $table->tinyInteger('car_arrival')->default(0)->comment('1->Arrived, 0->Not Arrived');
            $table->text('trip_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
