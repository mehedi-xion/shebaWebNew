<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('work',100);
            $table->string('home',100);
            $table->string('home_lat',25)->nullable();
            $table->string('home_long',25)->nullable();
            $table->string('work_lat',25);
            $table->string('work_long',25);
            $table->string('name',25);
            $table->tinyInteger('type')->comment('1=>home,2=>work');
            $table->tinyInteger('status')->default(1)->comment('1=>Active,2=>Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
