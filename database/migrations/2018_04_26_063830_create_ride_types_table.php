<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ride_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('image');
            $table->tinyInteger('max_seat');
            $table->double('fare');
            $table->double('min_fare');
            $table->double('km_fare');
            $table->double('time_fare');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ride_types');
    }
}
