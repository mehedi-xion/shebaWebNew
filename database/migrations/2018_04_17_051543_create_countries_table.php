<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_countries');
            $table->string('name');
            $table->string('iso_alpha2');
            $table->string('iso_alpha3');
            $table->mediumInteger('iso_numeric');
            $table->string('currency_code',3);
            $table->string('currency_name', 32);
            $table->string('currency_symbol', 3);
            $table->string('flag', 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
