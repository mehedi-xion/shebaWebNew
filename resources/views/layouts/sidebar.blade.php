<aside id="aside" class="app-aside hidden-xs bg-dark">
    <div class="aside-wrap">
        <div class="navi-wrap">
            <!-- nav -->
            <nav ui-nav class="navi clearfix">
                <ul class="nav">
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span>Navigation</span>
                    </li>

                    <li>
                        <a href="{{ route('dashboard') }}" class="auto">
                            <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                            <span class="font-bold">Dashboard</span>
                        </a>
                    </li>


                    <li class="line dk"></li>


                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span>Components</span>
                    </li>


                        <li class="@if( Request::segment(1)  == 'bookings') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="fa fa-check-circle"></i>
                            <span>Bookings</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{ route('all.bookings') }}"><span>All Bookings</span></a>
                            </li>
                            <li>
                                <a href="{{ route('split.bookings', 'completed') }}"><span>Completed</span></a>
                            </li>
                            <li>
                                <a href="{{ route('split.bookings', 'processing') }}"><span>On Processing</span></a>
                            </li>
                            <li>
                                <a href="{{ route('split.bookings', 'cancelled') }}"><span>Cancelled</span></a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="{{route('all.customers')}}" class="auto">
                            <i class="glyphicon glyphicon-user"></i>
                            <span>Customer</span>
                        </a>
                    </li>


                    <li class="@if( Request::segment(1)  == 'drivers') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                          <i class="fa fa-car"></i>
                            <span>Driver</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('drivers.create')}}"><span>Create Driver</span></a>
                            </li>
                            <li>
                                <a href="{{route('all.drivers')}}"><span>All Driver</span></a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="{{ route('all.transaction') }}">
                            <i class="glyphicon glyphicon-transfer"></i>
                            <span>Transaction</span>
                        </a>
                    </li>


                    <li class="@if( Request::segment(1)  == 'rides') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="fa fa-cubes"></i>
                            <span>Ride Type</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{ route('rides.create') }}"><span>Create Ride Type</span></a>
                            </li>
                            <li>
                                <a href="{{ route('rides.index') }}"><span>View Ride</span></a>
                            </li>
                        </ul>
                    </li>


                    <li class="@if( Request::segment(1)  == 'patterns') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="fa fa-sliders"></i>
                            <span>Pattern</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('patterns.create')}}"><span>Create Pattern</span></a>
                            </li>
                            <li>
                                <a href="{{route('patterns.index')}}"><span>View Pattern</span></a>
                            </li>
                        </ul>
                    </li>


                    <li class="@if( Request::segment(1)  == 'patterns') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="fa fa-life-ring"></i>
                            <span>Help Text</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('helps.create')}}"><span>Create Help Text</span></a>
                            </li>
                            <li>
                                <a href="{{route('helps.index')}}"><span>View Help Text</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="@if( Request::segment(1)  == 'promocodes') active @endif">
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="glyphicon glyphicon-barcode"></i>
                            <span>Promocode</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('promocodes.create')}}"><span>Create Promocodes</span></a>
                            </li>
                            <li>
                                <a href="{{route('promocodes.index')}}"><span>View Promocodes</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="@if( Request::segment(1)  == 'documents') active @endif">
                        <a href="{{route('documents.index')}}" class="auto">
                            <i class="glyphicon glyphicon-file"></i>
                            <span>Document</span>
                        </a>
                    </li>
                    <li class="@if( Request::segment(1)  == 'feedbacks') active @endif">
                        <a href="{{route('feedbacks.index')}}" class="auto">
                            <i class="glyphicon glyphicon-comment"></i>
                            <span>Feedback</span>
                        </a>
                    </li>

                    <li class="@if( Request::segment(1)  == 'requests') active @endif">
                        <a href="{{route('requests.index')}}" class="auto">
                            <i class="glyphicon glyphicon-refresh"></i>
                            <span>Requests</span>
                        </a>
                    </li>
                    <li>
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="glyphicon glyphicon-bell"></i>
                            <span>Notification</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('notifications.fcm')}}"><span>FCM Key</span></a>
                            </li>
                            <li>
                                <a href="{{route('notifications.index')}}"><span>All Notification</span></a>
                            </li>
                            <li>
                                <a href="{{route('notifications.create')}}"><span>Send Notification</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href class="auto">
                          <span class="pull-right text-muted">
                              <i class="fa fa-fw fa-angle-right text"></i>
                              <i class="fa fa-fw fa-angle-down text-active"></i>
                          </span>
                            <i class="fa fa-cogs"></i>
                            <span>Settings</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li>
                                <a href="{{route('settings.edit')}}"><span>General Settings</span></a>
                            </li>
                            <li>
                                <a href="layout_app.html"><span>Affiliate Settings</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk hidden-folded"></li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span>Your Stuff</span>
                    </li>
                    <li>
                        <a href="page_profile.html">
                            <i class="icon-user icon text-success-lter"></i>
                            <b class="badge bg-success pull-right">30%</b>
                            <span>Profile</span>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- nav -->
            <!-- aside footer -->
            <div class="wrapper m-t">
                <div class="text-center-folded">
                    <span class="pull-right pull-none-folded">60%</span>
                    <span class="hidden-folded">Milestone</span>
                </div>
                <div class="progress progress-xxs m-t-sm dk">
                    <div class="progress-bar progress-bar-info" style="width: 60%;">
                    </div>
                </div>
            </div>
            <!-- / aside footer -->
        </div>
    </div>
</aside>