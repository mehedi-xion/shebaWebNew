<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{\App\Setting::getHeaderText()}}</title>

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('libs/assets/animate.css/animate.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('libs/assets/simple-line-icons/css/simple-line-icons.css') }}"
          type="text/css"/>
    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/font.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" type="text/css"/>

</head>
<body style='background-image: url("{{ asset('assets/img/login_background.jpg') }}"); background-size: cover; background-position: top center;"'>

<div class="app app-header-fixed ">
    <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController"
         ng-init="app.settings.container = false;">
        <a href class="navbar-brand block m-t">Login</a>
        <div class="m-b-lg">
            <div class="wrapper text-center">
                <strong>Please Log in to get in touch</strong>
            </div>
            <form method="POST" action="{{ route('login') }}" class="form-validation">
                @csrf

                <div class="text-danger wrapper text-center" ng-show="authError">

                </div>
                <div class="list-group list-group-sm">

                    <div class="list-group-item">
                        <input id="email" type="email" ng-model="user.email"
                               class="form-control no-border {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" placeholder="Email"
                               value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="list-group-item">
                        <input id="password" type="password" ng-model="user.password"
                               class="no-border form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password" placeholder="Password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                </div>
                <div class="checkbox m-b-md m-t-none text-white">
                    <label class="i-checks">
                        <input type="checkbox" name="remember" ng-model="agree"><i></i>{{ __('Remember Me') }}
                    </label>
                </div>

                <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()"
                        ng-disabled='form.$invalid'>Log in
                </button>

                <div class="text-center m-t m-b">
                    <a href="{{ route('password.request') }}" class="btn-success btn-sm btn" ui-sref="access.forgotpwd">Forgot password?</a>
                </div>


            </form>
        </div>
        <div class="text-center" ng-include="'tpl/blocks/page_footer.html'">
            <p>
                <small class="text-muted">{{\App\Setting::getFooterText()}}</small>
            </p>
        </div>
    </div>


</div>

<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/ui-load.js') }}"></script>
<script src="{{ asset('assets/js/ui-jp.config.js') }}"></script>
<script src="{{ asset('assets/js/ui-jp.js') }}"></script>
<script src="{{ asset('assets/js/ui-nav.js') }}"></script>
<script src="{{ asset('assets/js/ui-toggle.js') }}"></script>
<script src="{{ asset('assets/js/ui-client.js') }}"></script>
</body>
</html>
