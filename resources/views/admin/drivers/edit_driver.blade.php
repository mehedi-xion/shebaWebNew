@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Edit Driver</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Edit Data of {{$driver->driver_name}} </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('all.driver.update', $driver->_id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method('PUT')

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input name="driver_name" required value="{{$driver->driver_name}}" type="text" class="form-control" placeholder="Please Enter Driver Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Ride Type</label>
                                <div class="col-lg-8">
                                    <select name="ride_id" required class="form-control">
                                        <option value=""></option>
                                        @foreach($rideTypes as $ridetype)
                                            <option value="{{$ridetype->_id}}" @if($ridetype->_id=== $driver->ride_id) selected='selected' @endif>{{$ridetype->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Phone</label>
                                <div class="col-lg-8">
                                    <input name="phone" required value="{{$driver->phone}}" type="text" class="form-control" placeholder="Please Enter Driver Phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Email</label>
                                <div class="col-lg-8">
                                    <input name="email" required value="{{$driver->email}}" type="text" class="form-control" placeholder="Please Enter Driver Email">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Address</label>
                                <div class="col-lg-8">
                                    <input name="address" value="{{$driver->address}}" type="text" class="form-control" placeholder="Please City">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Post Code</label>
                                <div class="col-lg-8">
                                    <input name="post_code" value="{{$driver->post_code}}" type="text" class="form-control" placeholder="Please Enter State">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Is Deaf</label>
                                <div class="col-lg-8">
                                    <select name="is_deaf" required class="form-control">
                                        <option value=""></option>
                                        <option @if($driver->is_deaf == 0) selected='selected' @endif value="0">No</option>
                                        <option @if($driver->is_deaf == 1) selected='selected' @endif value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Image</label>
                                <div class="col-lg-8">
                                    <input name="image" ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-primary'}" type="file">
                                    <hr>
                                    <img src="{{asset('storage/driver/'.$driver->image)}}" width="100px;" height="100px" />
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Ride model</label>
                                <div class="col-lg-8">
                                    <input name="ride_model" value="{{$driver->ride_model}}" type="text" class="form-control" placeholder="Please Enter Ride Model">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Registration No</label>
                                <div class="col-lg-8">
                                    <input name="vehicle_reg_no" value="{{$driver->vehicle_reg_no}}" type="text" class="form-control" placeholder="Please Enter Registration Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Licence Number</label>
                                <div class="col-lg-8">
                                    <input name="license_no" value="{{$driver->license_no}}" type="text" class="form-control" placeholder="Please Enter Licence Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Driver Type</label>
                                <div class="col-lg-8">
                                    <select name="driver_type" required class="form-control">
                                        <option value=""></option>
                                        <option @if($driver->driver_type == 0) selected='selected' @endif value="0">Driver Also An Owner</option>
                                        <option @if($driver->driver_type == 1) selected='selected' @endif value="1">Non Driving Partner</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">City</label>
                                <div class="col-lg-8">
                                    <input name="city" value="{{$driver->city}}" type="text" class="form-control" placeholder="Please City">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">State</label>
                                <div class="col-lg-8">
                                    <input name="state" value="{{$driver->state}}" type="text" class="form-control" placeholder="Please Enter State">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Is Flash Needed</label>
                                <div class="col-lg-8">
                                    <select name="is_deaf" required class="form-control">
                                        <option value=""></option>
                                        <option @if($driver->is_flash_required == 0) selected='selected' @endif value="0">No</option>
                                        <option @if($driver->is_flash_required == 1) selected='selected' @endif value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Status</label>
                                <div class="col-lg-8">
                                    <select name="status" required class="form-control">
                                        <option value=""></option>
                                        <option @if($driver->status == 0) selected='selected' @endif value="0">Inactive</option>
                                        <option @if($driver->status == 1) selected='selected' @endif value="1">Active</option>
                                        <option @if($driver->status == 2) selected='selected' @endif value="2">Deleted</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Update Driver</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection
