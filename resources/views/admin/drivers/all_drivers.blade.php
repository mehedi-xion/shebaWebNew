@extends('layouts.app')

@section('content')

    <div class="row bg-light lter b-b wrapper-md">
        <div class="col-md-12">
            <h1 class="m-n font-thin h3">Dashboard / All Drivers</h1>
        </div>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Drivers List</h4>
            </div>
            <div class="table-responsive">
                <table id="driver_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('all.drivers.json')}}',
              aoColumns: allDriverColumns
        }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-1">Name</th>
                        <th class="col-md-1">Affiliate<br>Count</th>
                        <th class="col-md-1">Ride Type</th>
                        <th class="col-md-1">Ride Model</th>
                        <th class="col-md-1">Balance</th>
                        <th class="col-md-1">Affiliate<br>Code</th>
                        <th class="col-md-1">Rating</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="driverDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Details of Driver</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="driver_name"></span></div>
                        <table id="modal_driver_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allDriverColumns = [
            {mData: 'driver_name'},
            {
                mData: 'driver_affiliate_count',
                render: function (data, type, row, meta) {
                    return '<span class="label bg-primary">D : ' + data + '</span>' +
                        '<span class="label label-primary">C : ' + row['customer_affiliate_count'] + '</span>';
                }
            },
            {mData: 'ride_name'},
            {mData: 'ride_model'},
            {mData: 'balance_amount'},
            {mData: 'affiliate_code'},
            {
                mData: 'driver_rating',
                render: function (data, type, row, meta) {
                    return '<i class="fa fa-star text-warning m-r-xs"></i>' + data;
                }
            },
            {
                mData: 'status',
                render: function (data, type, row, meta) {
                    return '<span class="label label-' + row['label_colors'] + '">' + data + '</span>';
                }
            },
            {
                mData: '_id',
                render: function (data, type, row, meta) {
                    return '<button id="driverDetailsModal" type="button" class="btn btn-sm btn-primary btn-addon"' +
                        'data-toggle="modal" data-target="#driverDetails"' +
                        'data-id="' + data + '">Details</button>' +
                        '' +
                        '<a class="btn btn-sm btn-dark"' +
                        'href="{{url("/drivers")}}/' + data + '/edit">Edit</a>' +
                        '' +
                        '<a class="btn btn-sm btn-success"' +
                        'href="{{url("/drivers")}}/' + data + '/payment">Payment</a>' +
                        '' +
                        '<form action="{{url("/drivers")}}/' + data + '" method="post" style=" display: inline;">' +
                        '<input type="hidden" name="_method" value="delete">' +
                        '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                        '<button onclick="return confirm(\'Are you sure?\')" class="btn btn-sm btn-danger" type="submit">Delete</button>' +
                        '</form>';
                }
            },
        ];

        $('table#driver_table').on('click', 'button#driverDetailsModal', function () {
            var id = $(this).data('id');

            $.get("{{url('drivers-json')}}/" + id, function (response) {

                var generatedTableHtml = '';
                $.each(response, function (key, value) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase() + keySting.substr(1);

                    generatedTableHtml += '<tr><td>' + UpperCaseKey + '</td>' +
                        '<td>' + value + '</td></tr>';
                });

                $('#driverDetails table#modal_driver_details tbody').html(generatedTableHtml)

            }, "json");
        })
    </script>
@endsection