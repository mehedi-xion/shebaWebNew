@extends('layouts.app')


@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Update Settings</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Existing Settings Info</h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('settings.update',$settings->_id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">App Name / Title</label>
                                <div class="col-lg-8">
                                    <input name="title"  value="{{$settings->title}}" required type="text" class="form-control" placeholder="Please Enter App Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Admin Header Title</label>
                                <div class="col-lg-8">
                                    <input name="header_title"  value="{{$settings->header_title}}" required type="text" class="form-control" placeholder="Please Enter Header title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Admin Footer Text</label>
                                <div class="col-lg-8">
                                    <input name="footer_text"  value="{{$settings->footer_text}}" required type="text" class="form-control" placeholder="Please Enter Footer Text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Tax</label>
                                <div class="col-lg-8">
                                    <input name="tax"  value="{{$settings->tax}}" required type="number" class="form-control" placeholder="Please Enter Tax">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Admin commission</label>
                                <div class="col-lg-8">
                                    <input name="admin_charge"  value="{{$settings->admin_charge}}" required type="number" class="form-control" placeholder="Please Enter Admin Commission">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Booking Code</label>
                                <div class="col-lg-8">
                                    <input name="booking_code"  value="{{$settings->booking_code}}" required type="text" class="form-control" placeholder="Please Enter Booking Code">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">API Key</label>
                                <div class="col-lg-8">
                                    <input name="browser_api_key"  value="{{$settings->browser_api_key}}" required type="text" class="form-control" placeholder="Please Enter API key">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">App Logo</label>
                                <div class="col-lg-8">
                                    <input name="image" ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-dark'}" type="file">
                                    <hr>
                                    <img src="{{asset('storage/images/'.$settings->logo)}}" width="100px;" height="100px" />
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update Settings</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('libs/jquery/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('input[name="start_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
        $('input[name="expiration_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
    </script>

@endsection