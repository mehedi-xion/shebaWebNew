@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / All Customer</h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Customer List</h4>
            </div>
            <div class="table-responsive">
                <table id="customer_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('all.customers.json')}}',
              aoColumns: allCustomerColumns
        }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-1">Affiliate<br>Count</th>
                        <th class="col-md-1">Phone</th>
                        <th class="col-md-1">Affiliate<br>Code</th>
                        <th class="col-md-1">No Of Bookings</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="customerDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Details of Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="customer_name"></span></div>
                        <table id="modal_customer_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allCustomerColumns = [
            {mData: 'name'},
            {
                mData: 'driver_affiliate_count',
                render: function (data, type, row, meta) {
                    return  '<span class="label bg-primary">D : '+data+'</span>' +
                            '<span class="label label-primary">C : '+row['customer_affiliate_count']+'</span>';
                }
            },
            {mData: 'phone'},
            {mData: 'affiliate_code'},
            {mData: 'number_of_bookings'},
            {
                mData: 'status',
                render: function (data, type, row, meta) {
                    return '<span class="label label-'+row['label_colors']+'">'+data+'</span>';
                }
            },
            {
                mData: '_id',
                render: function (data, type, row, meta) {
                    return '<button id="customerDetailsModal" type="button" class="btn btn-sm btn-primary"' +
                        'data-toggle="modal" data-target="#customerDetails"' +
                        'data-id="'+data+'">Details</button>' +
                        '' +
                        '<a class="btn btn-sm btn-dark"' +
                        'href="{{url("/customers")}}/' + data + '/edit">Edit</a>' +
                        '' +
                        '<form action="{{url("/customers")}}/' + data + '" method="post" style=" display: inline;">' +
                        '<input type="hidden" name="_method" value="delete">' +
                        '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                        '<button onclick="return confirm(\'Are you sure?\')" class="btn btn-sm btn-danger" type="submit">Delete</button>' +
                        '</form>';
                }
            },
        ];

        $('table#customer_table').on('click', 'button#customerDetailsModal', function () {
            var id = $(this).data('id');

            $.get( "{{url('customers-json')}}/"+id, function( response ) {

                var generatedTableHtml = '';
                $.each( response, function( key, value ) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase()+keySting.substr(1);

                    generatedTableHtml +=   '<tr><td>'+UpperCaseKey+'</td>' +
                                            '<td>'+value+'</td></tr>'
                });

                $('#customerDetails table#modal_customer_details tbody').html(generatedTableHtml)

            }, "json" );
        })
    </script>
@endsection