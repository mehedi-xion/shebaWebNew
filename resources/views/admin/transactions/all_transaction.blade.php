@extends('layouts.app')

@section('content')

    <div class="row bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / All Transactions</h1>

    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Transactions List</h4>
            </div>
            <div class="table-responsive">
                <table id="transaction_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('all.transaction.json')}}',
                    aoColumns: allTransactionColumns }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-1">Type</th>
                        <th class="col-md-1">Amount</th>
                        <th class="col-md-1">Purpose</th>
                        <th class="col-md-1">Comments</th>
                        <th class="col-md-1">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allTransactionColumns = [
            {
                mData: 'trans_user_type',
                render: function (data, type, row, meta) {
                    if(data){
                        return data;
                    }else{
                        return '';
                    }
                }
            },
            {mData: 'trans_amount'},
            {mData: 'trans_purpose'},
            {mData: 'comments'},
            {
                mData: 'trans_date',
                render: function (data, type, row, meta) {
                    return  new Date(data).toDateString();
                }

            },
        ];
    </script>
@endsection