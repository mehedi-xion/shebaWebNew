@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Payment</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <div class="h4 m-t-xs m-b-xs">
                        Add Payment for {{$driver->driver_name}}
                        <small class="pull-right">
                            Previous Due Status : <span class="label text-sm bg-black-opacity">{{ isset($driver->balance->amount) ? $driver->balance->amount : '0' }}</span> tk
                        </small>
                    </div>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('driver.payment.store', $driver->_id)}}" method="post"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Amount</label>
                                <div class="col-lg-8">
                                    <input name="amount" required type="text" class="form-control"
                                           placeholder="Please Enter Payment Amount">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Invoice Type</label>
                                <div class="col-lg-8">
                                    <select name="invoice_type" required class="form-control">
                                        <option value="1">Received From Driver</option>
                                        <option value="2">Pay To Driver</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Comments</label>
                                <div class="col-lg-8">
                                    <textarea name="comments" type="text" class="form-control"
                                              placeholder="Please Enter Comments"></textarea>
                                </div>
                            </div>


                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Create New Payment</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
