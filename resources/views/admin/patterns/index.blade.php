@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md row">
        <div class="col-md-10">
            <h1 class="m-n font-thin h3">Dashboard / All Pattern Types</h1>
        </div>
        <div class="col-md-2">
            <a href="{{route('patterns.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create New Pattern</a>
        </div>
    </div>

    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                View All pattern Types
            </div>
            <div class="panel-body">
                <table id="pattern-type-data" class="table table-bordered">
                    <thead>
                    <tr>

                        <th class="col-md-1">ID</th>
                        <th class="col-md-1">Name</th>
                        <th class="col-md-1">Ride Type</th>
                        <th class="col-md-1">Location</th>
                        <th class="col-md-1">Base Price</th>
                        <th class="col-md-1">Minimum Ride Fare</th>
                        <th class="col-md-1">KM Rate</th>
                        <th class="col-md-1">Min Rate</th>
                        <th class="col-md-1">Range</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($patterns as $pattern)
                    <tr>

                        <td>{{$loop->iteration}}</td>
                        <td>{{$pattern->name}}</td>
                        <td>{{(!empty($pattern->rideType) ? $pattern->rideType->name : '')}}</td>
                        <td>
                            {{isset($pattern->location['coordinates']) ? $pattern->location['coordinates'][0] : ''}}
                            <br>
                            {{isset($pattern->location['coordinates']) ? $pattern->location['coordinates'][1] : ''}}
                        </td>
                        <td>{{$pattern->base_price}} {{$pattern->currency}}</td>
                        <td>{{$pattern->min_ride_fare}}</td>
                        <td>{{$pattern->km_rate}}</td>
                        <td>{{$pattern->min_rate}}</td>
                        <td>{{$pattern->range}}</td>
                        <td>
                            @if($pattern->status == 0)
                                <label class="label bg-warning">Inactive</label>
                            @elseif($pattern->status == 1)
                                <label class="label bg-success">Active</label>
                            @elseif($pattern->status == 2)
                                <label class="label bg-danger">Deleted</label>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="{{route('patterns.edit', $pattern->_id)}}" class="btn btn-sm btn-dark">Edit</a>
                                <form action="{{route('patterns.destroy', $pattern->_id)}}" method="post" style=" display: inline;">
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection