<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">All Driver's Documents</h4>
</div>
<div class="modal-body">
    <div class="panel-group">

        @foreach( $documents as $document)
            <div class="panel panel-{{$allCssColor[$document->status]}}">
                <div class="panel-heading">
                    {{\App\DriverDocument::documentTypeName($document->type)}}
                </div>
                <div class="panel-body text-center">
                    @if(isset($document->image) && !empty($document->image))
                        <img class="img-responsive" src="{{asset('storage/driver_document/'.$document->image)}}"
                             alt="driver document">
                    @else
                        <img class="img-responsive" src="{{asset('storage/images/no_image.jpg')}}" alt="driver document" style="width: 130px; height: auto;">
                    @endif
                </div>
                <div class="panel-footer text-center">
                    <div class="label label-{{$allCssColor[$document->status]}}"> Status : {{\App\DriverDocument::getAllStatusAsTextOfDocuments()[$document->status]}}</div>
                </div>
            </div>

        @endforeach
    </div>
</div>
<div class="modal-footer">
    <div class="text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
    </div>

</div>