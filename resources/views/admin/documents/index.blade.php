@extends('layouts.app')

@section('content')

    <div class="row bg-light lter b-b wrapper-md">
        <div class="col-md-12">
            <h1 class="m-n font-thin h3">Dashboard / All Drivers Documents</h1>
        </div>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Drivers Documents List</h4>
                <small id="show_response" class="text-info"></small>
            </div>
            <div class="panel-body table-responsive">
                <table id="driver_document_table" ui-jq="dataTable"
                       ui-options="{sAjaxSource: '{{route('driver-documents.json')}}',
              aoColumns: allDriverColumns
        }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-9 text-center">Driver document</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="driverDocumentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>


    <div class="modal fade" id="singleDocumentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allDriverColumns = [
            {mData: 'driver_name'},
            {mData: 'driver_document'},
            {
                mData: '_id',
                render: function (data, type, row, meta) {
                    return '<button id="driverDocument" type="button"' +
                        'class="btn btn-sm btn-block btn-dark btn-addon"' +
                        'data-toggle="modal" data-target="#driverDocumentModal"' +
                        'data-id="' + data + '">Details</button>';
                }
            },
        ];

        $('table#driver_document_table').on('click', 'button#driverDocument', function () {
            var id = $(this).data('id');

            $.get("{{url('driver-documents')}}/" + id, function (response) {

                $('#driverDocumentModal .modal-content').html(response);
            });
        })

        var documentBtn;

        $('table#driver_document_table').on('click', 'span.singleDocument', function () {

            documentBtn = $(this);
            var id = documentBtn.data('id');

            $.get("{{url('documents')}}/" + id, function (response) {

                $('#singleDocumentModal .modal-content').html(response);

            });
        });

        $('#singleDocumentModal').on('submit', 'form#documents_update', function (event) {

            event.preventDefault();
            var form = $(this);
            var formData = form.serialize();
            var url = form.attr('action');

            $.post( url, formData, function( responses ) {

                documentBtn.removeClass();
                documentBtn.addClass('btn singleDocument m-l-xs m-b-xs btn-xs');
                documentBtn.addClass('btn-' + responses.css_color);
                $('small#show_response').html(responses.message);

                $("#singleDocumentModal").modal("hide");

            }, "json");


        })

    </script>
@endsection