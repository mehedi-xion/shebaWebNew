<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    <div class="text-center modal-title">
        <h4>Driver : {{$document->driver->driver_name}}</h4>
        <h5>{{$document->type_name}}</h5>
    </div>
</div>

<div class="modal-body">
    <img src="{{asset('storage/driver_document/'.$document->image)}}" class="img-responsive img-thumbnail" alt="">
</div>
<div class="modal-footer">
    <div class="text-center row" style="padding:20px">
        <form id="documents_update" action="{{route('documents.update', $document->_id)}}">
            {{ csrf_field() }}

            @method('PUT')
            <div class="col-md-4 col-md-offset-2">
                <select name="status" class="form-control bg-{{$document->status_css}}">
                    <option @if($document->status == 0) selected @endif value="0">Not Defined</option>
                    <option @if($document->status == 1) selected @endif value="1">Pending</option>
                    <option @if($document->status == 2) selected @endif value="2">Approve</option>
                    <option @if($document->status == 3) selected @endif value="3">Rejected</option>
                </select>
            </div>
            <div class="col-md-4">
                <div class="btn-group">
                    <button class="btn btn-{{$document->status_css}}" type="submit">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                </div>
            </div>


        </form>
    </div>
</div>
