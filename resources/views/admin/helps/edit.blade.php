@extends('layouts.app')
@section('style')

    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.min.css') }}" type="text/css"/>
@endsection
@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Update Help</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Update A Help </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('helps.update', $help->_id)}}" method="post" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Head</label>
                                <div class="col-lg-10">
                                    <input name="head"  value="{{ $help->head}}" required type="text" class="form-control" placeholder="Please Enter Help Head">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <select name="status" required class="form-control">
                                        <option @if($help->status == 1) selected='selected' @endif  value="1">Active</option>
                                        <option @if($help->status == 0) selected='selected' @endif   value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Content</label>
                                <div class="col-lg-10">
                                    <textarea name="contents" class="form-control">{{$help->content }}</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-2 control-label">Upload Image</label>
                                <div class="col-lg-4">
                                    <input name="image"  ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-info'}" type="file">
                                </div>
                                <div class="col-lg-6">
                                    <img src="{{asset('storage/images/'.$help->image)}}" width="100px;" height="100px" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update Help</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.min.js') }}"></script>
    <script>
        $('textarea[name="contents"]').wysihtml5();
    </script>

@endsection

