@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md row">
        <div class="col-md-10">
            <h1 class="m-n font-thin h3">Dashboard / All Help Text</h1>
        </div>
        <div class="col-md-2">
            <a href="{{route('helps.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create New Help</a>
        </div>
    </div>

    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                View All help Text
            </div>
            <div class="panel-body">
                <table id="help-type-data" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-1">Head</th>
                        <th class="col-md-2">Ratings</th>
                        <th class="col-md-3">Image</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($helpTable as $help)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$help->head}}</td>
                        <td>Like : {{$help->likes()}} || Dislikes :  {{$help->dislike()}}</td>
                        <td>
                            @if(!empty($help->image))
                                <img src="{{asset('storage/images/'.$help->image)}}"  style="width: 130px;height:auto;" alt="help image">
                            @else
                                <img src="{{asset('storage/images/no_image.jpg')}}"  style="width: 130px;height:auto;" alt="help image">
                            @endif
                        </td>
                        <td>
                            @if($help->status == 0)
                                <label class="label bg-warning">Inactive</label>
                            @elseif($help->status == 1)
                                <label class="label bg-success">Active</label>
                            @elseif($help->status == 2)
                                <label class="label bg-danger">Deleted</label>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <button id="helpDetailsModal" type="button" class="btn btn-sm btn-primary btn-addon"
                                        data-toggle="modal" data-target="#helpDetails" data-id="{{$help->_id}}">Details</button>

                                <a href="{{route('helps.edit', $help->_id)}}" class="btn btn-sm btn-dark">Edit</a>

                                <form action="{{route('helps.destroy', $help->_id)}}" method="post" style=" display: inline;">
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="modal fade" id="helpDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Details of Help Text</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="help_name"></span></div>
                        <table id="modal_help_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('table#help-type-data').on('click', 'button#helpDetailsModal', function () {
            var id = $(this).data('id');

            $.get("{{url('helps')}}/" + id, function (response) {

                var generatedTableHtml = '';
                $.each(response, function (key, value) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase() + keySting.substr(1);

                    generatedTableHtml += '<tr><td>' + UpperCaseKey + '</td>' +
                        '<td>' + value + '</td></tr>';
                });

                $('#helpDetails table#modal_help_details tbody').html(generatedTableHtml)

            }, "json");
        })
    </script>
@endsection