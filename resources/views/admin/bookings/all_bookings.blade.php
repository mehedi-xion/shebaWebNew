@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / All Bookings</h1>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Bookings List</h4>
            </div>
            <div class="table-responsive">
                <table ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('all.bookings.json')}}',
              aoColumns: allBookinsColumns
        }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-2">Source</th>
                        <th class="col-md-2">Destination</th>
                        <th class="col-md-1">Customer</th>
                        <th class="col-md-1">Driver</th>
                        <th class="col-md-1">Car Type</th>
                        <th class="col-md-2">Booking Date</th>
                        <th class="col-md-1">Amount</th>
                        <th class="col-md-1">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allBookinsColumns = [
            {mData: 'booking_id'},
            {mData: 'source'},
            {mData: 'destination'},
            {mData: 'customer_name'},
            {mData: 'driver_name'},
            {mData: 'car_name'},
            {mData: 'book_date'},
            {mData: 'fare'},
            {
                mData: 'status',
                render: function (data, type, row, meta) {
                    return '<span class="label label-'+row['label_colors']+'">'+data+'</span>';
                }
            },
        ];
    </script>
@endsection