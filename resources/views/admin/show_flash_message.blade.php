<div class="row">
    <div class="col-md-12">
        @if (session('success'))
            <div class="alert alert-success text-center">
                <b>{{ session('success') }}</b>
            </div>
        @elseif(session('error'))
            <div class="alert alert-warning text-center">
                <b>{{ session('error') }}</b>
            </div>
        @endif
    </div>
</div>