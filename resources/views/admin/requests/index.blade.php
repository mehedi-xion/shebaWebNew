@extends('layouts.app')

@section('content')

    <div class="row bg-light lter b-b wrapper-md">
        <div class="col-md-12">
            <h1 class="m-n font-thin h3">Dashboard / All Request</h1>
        </div>
    </div>

    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Request List</h4>
            </div>
            <div class="table-responsive">
                <table id="request_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('requests.json')}}',
              aoColumns: allRequestColumns, bProcessing: true}" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-1">Customer</th>
                        <th class="col-md-1">Driver</th>
                        <th class="col-md-3">Source</th>
                        <th class="col-md-3">Destination</th>
                        <th class="col-md-1">Fare Price</th>
                        <th class="col-md-1">Ride</th>
                        <th class="col-md-1">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="requestDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Request Details</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="driver_name"></span></div>
                        <table id="modal_request_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var allRequestColumns = [

            {
                mData: 'customer_name',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },
            {
                mData: 'driver_name',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },
            {
                mData: 'source',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },

            {
                mData: 'destination',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },

            {
                mData: 'total_amount',
                render: function (data, type, row, meta) {
                    return (data) ? data + ' Tk' : 0;
                }
            },
            {
                mData: 'status',
                render: function (data, type, row, meta) {
                    if(data == 1){
                        return '<span class="label label-success">Assigned</span>';
                    }else if(data == 2){
                        return '<span class="label label-danger">Failed</span>';
                    }else if(data == 3){
                        return '<span class="label label-warning">Cancelled</span>';
                    }else if(data == 4){
                        return '<span class="label label-info">Pending</span>';
                    }else{
                        return 'Undefined'
                    }
                }
            },

            {
                mData: '_id',
                render: function (data, type, row, meta) {
                    return '<button id="requestDetailModal" type="button" class="btn btn-sm btn-primary"' +
                        'data-toggle="modal" data-target="#requestDetail"' +
                        'data-id="' + data + '">Details</button>';
                }
            },
        ];




        $('table#request_table').on('click', 'button#requestDetailModal', function () {
            var id = $(this).data('id');

            $.get("{{url('requests')}}/" + id, function (response) {

                var generatedTableHtml = '';
                $.each(response, function (key, value) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase() + keySting.substr(1);

                    generatedTableHtml += '<tr><td>' + UpperCaseKey + '</td>' +
                        '<td>' + value + '</td></tr>';
                });

                $('#requestDetail table#modal_request_details tbody').html(generatedTableHtml)

            }, "json");
        })
    </script>
@endsection