@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Create Ride Type</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Create A New Ride Type </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('rides.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input name="name" required type="text" class="form-control" placeholder="Please Enter Ride Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Max Seat</label>
                                <div class="col-lg-8">
                                    <input name="max_seat" required type="number" class="form-control" placeholder="Please Enter Max Seat">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Fare</label>
                                <div class="col-lg-8">
                                    <input name="fare" required type="number" class="form-control" placeholder="Please Enter Ride Fare">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Minimum Fare</label>
                                <div class="col-lg-8">
                                    <input name="min_fare" required type="number" class="form-control" placeholder="Please Enter Minimum Fare">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Per KM Fare</label>
                                <div class="col-lg-8">
                                    <input name="km_fare" required type="number" class="form-control" placeholder="Please Enter KM Fare">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label"> Time Fare</label>
                                <div class="col-lg-8">
                                    <input name="time_fare" required type="number" class="form-control" placeholder="Please Time Fare">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Image</label>
                                <div class="col-lg-8">
                                    <input name="image"  ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-info'}" type="file">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Status</label>
                                <div class="col-lg-8">
                                    <select name="status" required class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Create New Driver</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
