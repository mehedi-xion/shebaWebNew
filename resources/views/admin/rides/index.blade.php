@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">

        <div class="col-md-10">
            <h1 class="m-n font-thin h3">Dashboard / All Ride Types</h1>
        </div>
        <div class="col-md-2">
            <a href="{{route('rides.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create New Rides</a>
        </div>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                View All ride Types
            </div>
            <div class="panel-body">
                <table id="ride-type-data" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-2">Ride Name</th>
                        <th class="col-md-1">Maximum Seats</th>
                        <th class="col-md-1">Fare</th>
                        <th class="col-md-1">Min Fare</th>
                        <th class="col-md-1">KM Fare</th>
                        <th class="col-md-1">Time Fare</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rides as $ride)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$ride->name}}</td>
                        <td>{{$ride->max_seat}}</td>
                        <td>{{$ride->fare}}</td>
                        <td>{{$ride->min_fare}}</td>
                        <td>{{$ride->km_fare}}</td>
                        <td>{{$ride->time_fare}}</td>
                        <td>
                            @if($ride->status == 0)
                                <label class="label bg-warning">Inactive</label>
                            @elseif($ride->status == 1)
                                <label class="label bg-success">Active</label>
                            @elseif($ride->status == 2)
                                <label class="label bg-danger">Deleted</label>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <button id="rideDetailsModal" type="button" class="btn btn-sm btn-primary"
                                        data-toggle="modal" data-target="#rideDetails" data-id="{{$ride->_id}}">View</button>
                                <a href="{{route('rides.edit', $ride->_id)}}" class="btn btn-sm btn-dark">Edit</a>
                                <form action="{{route('rides.destroy', $ride->_id)}}" method="post" style=" display: inline;">
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="modal fade" id="rideDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Details of Ride Type</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="ride_name"></span></div>
                        <table id="modal_ride_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        
        $('table#ride-type-data').on('click', 'button#rideDetailsModal', function () {
            var id = $(this).data('id');

            $.get("{{url('rides')}}/" +id, function (response) {

                var generatedTableHtml = '';
                $.each(response, function (key, value) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase() + keySting.substr(1);

                    generatedTableHtml += '<tr><td>' + UpperCaseKey + '</td>' +
                        '<td>' + value + '</td></tr>';
                });

                $('#rideDetails table#modal_ride_details tbody').html(generatedTableHtml)

            }, "json");
        })
    </script>
@endsection