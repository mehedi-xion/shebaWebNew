@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md row">
        <div class="col-md-10">
            <h1 class="m-n font-thin h3">Dashboard / All Promocode</h1>
        </div>
        <div class="col-md-2">
            <a href="{{route('promocodes.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create New Promocode</a>
        </div>
    </div>

    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                View All Promocodes
            </div>
            <div class="panel-body">
                <table id="promocode-type-data" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-md-1">SL</th>
                        <th class="col-md-1">Code</th>
                        <th class="col-md-2">Start Date</th>
                        <th class="col-md-2">Expiration Date</th>
                        <th class="col-md-1">Offer Amount</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($promocodes as $promocode)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$promocode->code}}</td>
                        <td>{{ date('d M Y', strtotime($promocode->start_date)) }}</td>
                        <td>{{ date('d M Y', strtotime($promocode->expiration_date)) }}</td>
                        <td>{{$promocode->off}}</td>

                        <td>
                            @if($promocode->status == 0)
                                <label class="label bg-warning">Inactive</label>
                            @elseif($promocode->status == 1)
                                <label class="label bg-success">Active</label>
                            @elseif($promocode->status == 2)
                                <label class="label bg-danger">Deleted</label>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('promocodes.edit', $promocode->_id)}}" class="btn btn-primary btn-xs btn-block">Edit</a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
