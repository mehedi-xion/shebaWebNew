@extends('layouts.app')

@section('style')

    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" type="text/css"/>
@endsection
@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Update Promocode</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Update A Promocode </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('promocodes.update', $promocode->_id)}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Code</label>
                                <div class="col-lg-10">
                                    <input name="code"  value="{{ $promocode->code }}" required type="text" class="form-control" placeholder="Please Enter Code">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Start Date</label>
                                <div class="col-lg-10">
                                    <input name="start_date"  value="{{ date('d-m-Y', strtotime($promocode->start_date)) }}" required type="text" class="form-control" placeholder="Please Enter Start date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Expiration Date</label>
                                <div class="col-lg-10">
                                    <input name="expiration_date"  value="{{ date('d-m-Y', strtotime($promocode->expiration_date)) }}" required type="text" class="form-control" placeholder="Please Enter Start date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Offer</label>
                                <div class="col-lg-10">
                                    <input name="offer"  value="{{ $promocode->offer }}" required type="text" class="form-control" placeholder="Please Enter Start date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <select name="status" required class="form-control">
                                        <option @if($promocode->status == 1) selected='selected' @endif  value="1">Active</option>
                                        <option @if($promocode->status == 0) selected='selected' @endif   value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        
                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update Promocode</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('libs/jquery/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('input[name="start_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
        $('input[name="expiration_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
    </script>

@endsection