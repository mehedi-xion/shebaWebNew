<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {

    $data = Auth::user();
    $data['whoAmI'] = "hi I am a Admin User";
    return $data;

});

//Route::middleware('auth:driver')->get('/driver',
//Route::middleware('auth:customer')->get('/customer',
//Route::middleware('auth:api')->get('/user',
//$data = Auth::user();


Route::prefix('v1')->group(function () {



    Route::prefix('driver')->group(function () {

        //http://shebaweb.test/api/v1/driver/login

        Route::post('login', 'V1\DriverEndApi\DriverAuthApiController@login');
        Route::post('register', 'V1\DriverEndApi\DriverAuthApiController@register');

        Route::middleware('auth:driver')->group(function () {

            Route::post('forgot', 'V1\DriverEndApi\DriverAuthApiController@forgot');

            Route::post('logout', 'V1\DriverEndApi\DriverAuthApiController@logout');

            Route::post('status', 'V1\DriverEndApi\DriverInfoApiController@getDriverStatus');

            Route::post('update_status', 'V1\DriverEndApi\DriverInfoApiController@updateDriverStatus');




            Route::get('/', function () {

                return Auth::user();
            });

        });

    });



    Route::prefix('customer')->group(function () {

        //http://shebaweb.test/api/v1/customer/login
        Route::post('login', 'V1\CustomerEndApi\CustomerAuthApiController@login');
        Route::post('register', 'V1\CustomerEndApi\CustomerAuthApiController@register');

        Route::post('forgot', 'V1\CustomerEndApi\CustomerAuthApiController@forgot');

        Route::middleware('auth:customer')->group(function () {


            Route::get('logout', 'V1\CustomerEndApi\CustomerAuthApiController@logout');

            Route::get('/', function () {

                return Auth::user();
            });

        });

    });

});



